require 'logger'

class LCDLogger
  def initialize(filename='log/lcdio.log')
    @file = File.open(filename, File::WRONLY | File::APPEND | File::CREAT)
    @file.sync = true
    @logger = Logger.new(@file, 10, 1024000)
    @logger.datetime_format = "%Y-%m-%d %H:%M:%S"
    @logger.formatter = proc do |severity, datetime, progname, msg|
      "[#{severity}] #{datetime}: #{msg}\n"
    end
    @logger.debug "Iniciando Logger"
    @logger
  end

  def info(msg)
    @logger.info msg
  end

  def warn(msg)
    @logger.warn msg
  end

  def debug(msg)
    @logger.debug msg
  end

  def close
    debug "Fechando Logger"
    @file.close
    @logger.close
  end
end
