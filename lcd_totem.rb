#!/usr/bin/env ruby
# encoding: UTF-8

require 'bundler/setup'
require 'ostruct'

require_relative 'environment'
require_relative 'settings'

require_relative 'lcd'

require_relative 'user_session'
require_relative 'slot'
require_relative 'station'

require_relative 'lcd_logger'

require_relative 'services/user_service'
require_relative 'managers/user_manager'

def main_loop
  $logger.info 'Iniciando loop principal'
  return $logger.info('Não encontrou usuário') unless find_cyclist_or_operator
  
  return $logger.info('Senha Inválida') unless find_password(3)

  return unless release_bike(3)

  release_more_bikes?
rescue Timeout::Error
  $logger.info('Tempo expirado')
end

def release_more_bikes?
  if $manager.user.type == 'Mechanical'
    return if cancel?($lcd.release_more_bikes?)

    release_bike(3)
    release_more_bikes?
  end
end

def find_cyclist_or_operator
  cpf = $lcd.wait_for_cpf
  return if cancel?(cpf)
  
  $manager = UserManager.new(cpf, $lcd)
  $manager.valid?
end

def find_password(tries)
  return false if tries.zero?

  password = $lcd.wait_for_password
  return if cancel?(password)

  $lcd.authenticating

  session = UserSession.authenticate($manager.user.login, password)

  return true if session != :connection_error && session.persisted?

  connection_error?(session, :invalid_password)
  find_password(tries - 1)
end

def release_bike(tries)
  return false if tries.zero?

  if Slot.all.blank?
    $lcd.no_available_slots
    return release_bike(tries - 1)
  end

  $selected_slot = $lcd.wait_for_slot_number
  return if cancel?($selected_slot)

  slot_release = Slot.release($selected_slot, $manager.user)

  if slot_release
    $lcd.press_button
    return true
  end

  $lcd.invalid_slot
  release_bike(tries - 1)
rescue Errno::ECONNREFUSED
  $lcd.no_available_slots
end

def cancel?(input)
  $logger.info('Operação Cancelada') if input == :cancel
  input == :cancel
end

def connection_error?(response, method_name)
  if response == :connection_error
    $lcd.connection_error
  else
    $lcd.send method_name
  end
end

def station_open?
  status = Station.status

  $lcd.station_closed unless status
   
  status
end

$logger = LCDLogger.new(File.dirname(__FILE__)+'/log/lcd.log')
$logger.info 'Iniciando display LCD'

$lcd = LCD.new($logger)

ActiveResource::Base.logger = $logger

at_exit { $logger.info 'Terminando a execução' }
main_loop
