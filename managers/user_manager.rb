class UserManager
  attr_accessor :user, :lcd

  LCD_MESSAGES = {
    'connection_error' => :connection_error,
    'internet_error' => :internet_error,
    'api_error' => :api_error,
    'not_found' => :invalid_cpf,
    'not_paid' => :inactive_cyclist,
    'blocked' => :blocked_cyclist,
    'cannot_rent' => :maximum_bikes_retrieved,
    'creditcard_expired' => :creditcard_expired,
    'station_closed' => :station_closed,
    'waiting_rent' => :waiting_rent
  }

  def initialize(cpf, lcd)
   @lcd  = lcd
   @lcd.finding_cyclist

   @user = UserService.find(cpf) 
  end

  def valid?
    display_message
    @user.valid
  end

  private

  def display_message
    @lcd.send(LCD_MESSAGES.fetch(@user.reason, :unknow_error_message)) unless @user.valid
  end
end