require 'rest-client'

module UserService
  extend self

  CHECK_INTERNET = 'ping -c 3 www.google.com.br > /dev/null 2>&1'

  def find(cpf)
    operator = find_operator(cpf)

    if not_found?(operator)
      find_cyclist(cpf)
    else
      operator
    end
    
  rescue RestClient::InternalServerError
    internet_connection? ? connection_error : internet_error
  rescue Errno::ECONNREFUSED
    api_error
  end

  private

  def connection_error
    OpenStruct.new(valid: false, reason: 'connection_error')
  end

  def internet_error
    OpenStruct.new(valid: false, reason: 'internet_error')
  end

  def api_error
    OpenStruct.new(valid: false, reason: 'api_error')
  end

  def not_found?(user)
    user.reason == 'not_found'
  end

  def find_cyclist(cpf)
    finder('cyclists', cpf)
  end

  def find_operator(cpf)
    finder('mechanicals', cpf)
  end

  # TODO Passar para uma classe/modulo específico
  def internet_connection?
    system(CHECK_INTERNET)
  end

  def finder(type, cpf)
    response = RestClient.get("#{Settings.api.path}/#{type}/#{cpf}", {:accept => :json})
    OpenStruct.new(JSON.parse(response))
  end
end