require 'settingslogic'

class Settings < Settingslogic
  current_path = File.expand_path(File.dirname(__FILE__))
  source "#{current_path}/config/api.yml"
  namespace WEB_TOTEM_ENV
  load!
end
