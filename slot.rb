require_relative "environment"
require_relative "local_resource"

class Slot < LocalResource

  def self.release(position, user)
    position = position.to_i
    # BUG: slot 10 and 17 are always broken,
    # so we skip the number 10 and 17 to 11 and 18
    position += 1 if position >= 10
    position += 1 if position >= 17

    return post("#{position}/release", cpf: user.cpf, role: user.type.downcase)
  rescue
    return false
  end
end
