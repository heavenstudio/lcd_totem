require_relative "../cpf_helper"

describe CPFHelper do
  describe "format" do
    it { CPFHelper::format("33943325822").should == "339.433.258-22" }
    it { CPFHelper::format("5822").should == "58-22" }
    it { CPFHelper::format("125822").should == "1.258-22" }
    it { CPFHelper::format("121125822").should == "1.211.258-22" }
  end
end