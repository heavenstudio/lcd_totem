require 'spec_helper'

describe UserManager do
  describe '#valid?' do
    let(:valid) {true}
    let(:reason) {''}
    let(:lcd) {double('LCD')}
    let(:response) {OpenStruct.new(valid: valid, reason: reason)}

    before do
      UserService.stub(:find) {response} 
      lcd.should_receive(:finding_cyclist)
    end

    subject {described_class.new('a_cpf_number', lcd)}

    context 'when is valid' do
      its(:valid?) {should be_true}
    end

    context 'when is invalid' do
      let(:valid) {false}

      context 'connection error' do
        let(:reason) {'connection_error'}

        it 'must show the message' do
          lcd.should_receive(:connection_error)
          subject.should_not be_valid
        end
      end

      context 'api error' do
        let(:reason) {'api_error'}

        it 'must show the message' do
          lcd.should_receive(:api_error)
          subject.should_not be_valid
        end
      end

      context 'internet error' do
        let(:reason) {'internet_error'}

        it 'must show the message' do
          lcd.should_receive(:internet_error)
          subject.should_not be_valid
        end
      end

      context 'not found' do
        let(:reason) {'not_found'}

        it 'must show the message' do
          lcd.should_receive(:invalid_cpf)
          subject.should_not be_valid
        end
      end

      context 'not paid' do
        let(:reason) {'not_paid'}

        it 'must show the message' do
          lcd.should_receive(:inactive_cyclist)
          subject.should_not be_valid
        end
      end

      context 'blocked' do
        let(:reason) {'blocked'}

        it 'must show the message' do
          lcd.should_receive(:blocked_cyclist)
          subject.should_not be_valid
        end
      end

      context 'cannot rent more bikes' do
        let(:reason) {'cannot_rent'}

        it 'must show the message' do
          lcd.should_receive(:maximum_bikes_retrieved)
          subject.should_not be_valid
        end
      end

      context 'credit card expired' do
        let(:reason) {'creditcard_expired'}

        it 'must show the message' do
          lcd.should_receive(:creditcard_expired)
          subject.should_not be_valid
        end
      end

      context 'station closed' do
        let(:reason) {'station_closed'}

        it 'must show the message' do
          lcd.should_receive(:station_closed)
          subject.should_not be_valid
        end
      end

      context 'waiting release or cancel the last rent' do
        let(:reason) {'waiting_rent'}

        it 'must show the message' do
          lcd.should_receive(:waiting_rent)
          subject.should_not be_valid
        end
      end

      context 'unknow error message' do
        let(:reason) {'unknow'}

        it 'must show the message' do
          lcd.should_receive(:unknow_error_message)
          subject.should_not be_valid
        end
      end
    end
  end
end