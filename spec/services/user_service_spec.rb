require 'spec_helper'

describe UserService do
  let(:cyclist) {'35277745807'}
  let(:mechanical) {'51631585118'}
  let(:not_found) {'82727266654'}

  describe 'user login' do
    context 'when raise an error' do
      context 'api is down' do
        subject do
          RestClient.stub(:get).and_raise(Errno::ECONNREFUSED)
          UserService.find(cyclist)
        end

        its(:reason){ should eq('api_error') }
      end

      context 'without outside comunication' do
        use_vcr_cassette "no_internet"

        context 'server error' do
          subject{ UserService.find(cyclist) }

          its(:reason){ should eq('connection_error') }
        end

        context 'internet error' do
          before {UserService.should_receive(:system).with(UserService::CHECK_INTERNET) {false}}
          subject{ UserService.find(cyclist) }

          its(:reason){ should eq('internet_error') }
        end
      end
    end

    context 'when is not found' do
      use_vcr_cassette "login_not_found"

      subject{ UserService.find(not_found) }

      its(:reason){ should eq('not_found') }
    end

    context 'when found' do
      context 'when is cyclist' do
        use_vcr_cassette "login_cyclist"

        subject{ UserService.find(cyclist) }

        its(:type){ should eq('Cyclist') }
        its(:valid){ should be_true }
      end

      context 'when is mechanical' do
        use_vcr_cassette "login_mechanical"

        subject{ UserService.find(mechanical) }

        its(:type){ should eq('Mechanical') }
        its(:valid){ should be_true }
      end
    end
  end
end