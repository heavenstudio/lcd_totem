require 'spec_helper'

describe Slot do
  describe "::create_post_url" do
    xit "should return '1/release?role=cyclist' for a cyclist with position 1" do
      user = Cyclist.new
      Slot.create_post_url(1, user).should == '1/release?role=cyclist'
    end

    xit "should return '1/release?role=mechanical' for a mechanical with position 1" do
      user = Mechanical.new
      Slot.create_post_url(1, user).should == '1/release?role=mechanical'
    end
  end
end
