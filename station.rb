require_relative "local_resource"

class Station < LocalResource

  def self.status
    get(:status)
  rescue ArgumentError
    nil
  rescue
    :connection_error
  end
end