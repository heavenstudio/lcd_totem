require_relative "remote_resource"

class UserSession < RemoteResource

  def self.authenticate(login, password)
    create login: login, password: password
  rescue
    :connection_error
  end
end